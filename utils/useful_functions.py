""" 
@author: Darlin KUAJO
Ce module contient les fonctionnalités utiles au developpement de W2VBRS"""

import os
import nltk
import string
import numpy as np
import pandas as pd
from tqdm import tqdm
from IPython.core.display import HTML

EN_STOPWORD = nltk.corpus.stopwords.words('english')
CSS = """
<style>
h1, th{
    text-align:center;
}
td{
    text-align:justify;
}
.dataframe{
    border-collapse:collapse;
    margin: 0 auto;
}
th, td{
    padding: 10px;
}
</style>
\n
"""

def getcategories(asin, metadata):
    """Cette fonction retoune la liste des catégories du produit identifié par asin
    
    asin : str
    metadata : pd.DataFrame
    
    Valeur de retour : list(str)
    """
    return metadata[metadata['asin'] == asin].reset_index(inplace=False, drop = True).iloc[0]['categories']

def getItemByIndex(index, metadata):
    """ Cette fonction permet de retourner l'identifiant d'un produit à partir de son index
    
    metadata : pd.DataFrame
    index : int
    
    index > 0
    
    Valeur de retour : str
    """
    return metadata.iloc[index]['asin']

def getUserByIndex(reviewerID, evaluation_matrix):
    """ Cette fonction permet de retourner l'identifiant d'un utilisateur à partir de son index
    
    evaluation_matrix : dict
    index : int
    
    index > 0
    
    Valeur de retour : str
    """
    users = list(evaluation_matrix.keys())
    return users[index]

def getdescription(asin, metadata):
    """Cette fonction retoune la description textuelle du produit identifié par asin
    
    asin : str
    metadata : pd.DataFrame
    
    Valeur de retour : str
    """
    return metadata[metadata['asin'] == asin].reset_index(inplace=False, drop = True).iloc[0]['description']

def imUrl_to_image_html_width100(imUrl):
    """Cette fonction permet de convertir l'adressse url de l'image d'un produit en
    balise HTML nécessaire pour afficher une image cliquable avec une largeur de 100px
    
    imUrl : str
    
    Valeur de retour : str
    """
    
    return '<a href="' + imUrl + '">' + '<img src="'+ imUrl + '" width="100px" >' + '</a>'


def visualisation_select_product(k, asin, metadata):
    """ Cette fonction est dédiée à l'affichage du produit sélectionné.
    
    k : int
    asin : str
    metadata : pd.DataFrame
    
    k >= 0
    
    Valeur de retour : IPython.core.display.HTML, IPython.core.display.HTML
    """
    
    # Création du DataFrame nécessaire pour la visualisation du produit choisi
    print("[INFO] Génération d'un affichage du produit sélectionné ...")
    item_df = metadata[metadata['asin'] == asin]
    item_df.reset_index(inplace=True, drop=True)
    item_df = item_df.loc[:,['asin', 'description', 'categories', 'imUrl']]
    item_df.rename(columns={'imUrl' : 'image'}, inplace=True)
    code_html = item_df.to_html(escape=False,index=False, formatters=dict(image=imUrl_to_image_html_width100))
    
    # Création d'un fichier .html à la racine du projet contenant une visualisation du produit sélectionné et la liste de recommandation
    print("[INFO] Création du fichier de visualisation ...")
    with open("top-" + str(k) + "-recommandation.html", 'w', encoding='UTF-8') as fichier:
        fichier.write(CSS)
        fichier.write(code_html.replace("""<table border="1" class="dataframe">""", 
                                        """ <table border="1" class="dataframe"> \n <caption><h1>Produit sélectionné</h1></caption>"""))
        fichier.write("\n")
        
    return HTML(item_df.to_html(escape=False, formatters=dict(image=imUrl_to_image_html_width100)))

def visualisation_recommended_products(k, recommendations_list, score_name, list_title, metadata):
    """ Cette fonction est dédiée à l'affichage du produit sélectionné et de la liste de recommandation
    
    k : int
    recommendations_list : list(flozt, str)
    name_score : str
    metadata : pd.DataFrame
    
    k >= 0
    
    Valeur de retour : IPython.core.display.HTML, IPython.core.display.HTML
    """
    
    # Création du DataFrame qui présentera la liste de recommandation
    print("[INFO] Génération d'un affichage de la liste de recommandation ...")
    score = []
    item = []
    for i in range(len(recommendations_list)):
        score.append(recommendations_list[i][0])
        item.append(recommendations_list[i][1])
    recommended_products = pd.DataFrame({"asin" : item, score_name : score})
    recommended_products = pd.merge(recommended_products, metadata, how='inner', on='asin')
    recommended_products = recommended_products.rename(columns={'imUrl':'image'})
    recommended_products = recommended_products.loc[:, ['asin', score_name, 'description', 'categories','image']]
    code_html = recommended_products.to_html(escape=False, formatters=dict(image=imUrl_to_image_html_width100))
    
    # Création d'un fichier .html à la racine du projet contenant une visualisation du produit sélectionné et la liste de recommandation
    print("[INFO] Mise à jour du fichier de visualisation ...")
    with open("top-" + str(k) + "-recommandation.html", 'a', encoding='UTF-8') as fichier:
        fichier.write(code_html.replace("""<table border="1" class="dataframe">""", 
                                        """ <table border="1" class="dataframe"> \n <caption><h1>""" + list_title + """</h1></caption>"""))
        fichier.write("\n")
        
    return HTML(recommended_products.to_html(escape=False, formatters=dict(image=imUrl_to_image_html_width100)))
                
def remove_punctuation(texte):
    """ Cette fonction permet de supprimer les caractères de ponctuation contenus dans un texte
    
    texte : str
    
    Valeur de retour : str
    """
    
    result = "".join( [ch for ch in texte if ch not in string.punctuation])
    
    return result

def tokenizer(texte):
    """ Cette fonction permet de tokéniser un texte 
    
    texte : str
    
    Valeur de retour : list(str)
    """
    
    words = texte.split()
    
    return words

def remove_stopwords(tokens_list):
    """ Cette fonction permet de supprimer les mots vides de la langue anglaise contenus dans une liste de tokens 
    
    tokens_list : list(str)
     
     Valeur de retour : list(str)
    """
    
    result = [word for word in tokens_list if word not in EN_STOPWORD]
    
    return result

def clear_description(description):
    """Cette fonction permet de prétraiter la description textuelle d'un produit
    
    Les opérations de prétraitement sont:
    - Conversion de la description en minuscule
    - Suppression des caractères de ponctuation
    - Tokénisation
    - Suppression des mots vides
    
    description : str
    
    Valeur de retour : list(str)
    """
    
    description = description.lower()
    description = remove_punctuation(description)
    description = tokenizer(description)
    description = remove_stopwords(description)
    
    return description

def sim_cosine(item_embedding1, item_embedding2):
    """Cette fonction calcul la similarité de cosinus entre les deux items embeddings item_embedding1 et  item_embedding2
    
    item_embedding1 : numpy.ndarray
    item_embedding2 : numpy.ndarray
    
    Valeur de retour : float
    """
    
    cosine = np.dot(item_embedding1, item_embedding2) / (np.linalg.norm(item_embedding1) * np.linalg.norm(item_embedding2))
    
    return cosine

def mean_pooling(matrix):
    """ Cette fonction calcul la moyenne colonne à colonne de la matrice matrix
    
    matrix : numpy.ndarray
    
    Valeur de retour : numpy.ndarray
    """
    
    return np.mean(matrix, axis=0)

def itemLT(asin, metadata, model):
    """ Cette fonction permet de renvoyer le item embedding de l'item asin
    
    asin : str
    model : gensim.models.word2vec.Word2Vec
    
    Valeur de retour : numpy.ndarray
    """
    
    description = getdescription(asin, metadata)
    tokens_list = clear_description(description)
    item_embedding = []
    for i in range(len(tokens_list)):
        item_embedding.append(model.wv[tokens_list[i]])
    item_embedding = np.array(item_embedding)
    item_embedding = mean_pooling(item_embedding)
    
    return item_embedding

def top_k_recommendation(asin, k, metadata,  model, similarity_mesure):
    """Cette fonction renvoie la liste des k premiers produits les plus similaires au produit asin en guise de liste de recommandation top-k, selon l'ordre décroissant des scores de similarité de ces produits par rapport au produit asin
    
    asin : str
    k : int
    data : pd.DataFrame
  
    model : gensim.models.word2vec.Word2Vec
    similarity_mesure : function
    
    k > 0
        
     Valeur de retour : list(IPython.core.display.HTML, IPython.core.display.HTML)
    """
    
    recommendations_list = []
    item_embedding1 =  itemLT(asin, metadata, model)
    categories = getcategories(asin, metadata)
    
    # Détermination des produits appartenant à la même catégorie que le prosuit asin
    print('[INFO] Détermination des produits appartenant à la même catégorie que le produit choisi...')
        
    same_product_category = []
    with tqdm(total=len(metadata.index)) as progressbar:
        for i in metadata.index:
            if metadata.iloc[i]['asin'] != asin and categories == metadata.iloc[i]['categories']:
                same_product_category.append(metadata.iloc[i]['asin']) 
            progressbar.update(1)
    same_product_category = pd.DataFrame({"asin" : same_product_category})
    same_product_category = pd.merge(metadata, same_product_category, how='inner', on='asin')
    
    # Calcul des scores de similarité entre le produit asin et tous les autres produits de la même catégorie
    print('[INFO]  Calcul des scores de similarité entre le produit choisi et tous les autres produits de la même catégorie ...')
    
    with tqdm(total=len(same_product_category.index)) as progressbar:
        for i in same_product_category.index:
            item_embedding2 = itemLT(same_product_category.iloc[i]['asin'], metadata, model)
            recommendations_list.append((similarity_mesure(item_embedding1, item_embedding2), same_product_category.iloc[i]['asin']))
            progressbar.update(1)
    
    # Tri des produits par ordre décroissant des scores de similarité
    print("[INFO] Tri de la liste par ordre décroissaant des scores de similarité ...")
    recommendations_list.sort(reverse=True)
    
    # Extraction des n premiers produits
    if len(recommendations_list) > k:
        recommendations_list = recommendations_list[0:k]
    
    return (visualisation_select_product(k, asin, metadata),
            visualisation_recommended_products(k, recommendations_list, "cosine_similarity", "Produits similaires", metadata)
           )


